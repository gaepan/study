import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, StyleSheet} from 'react-native';
import { FontAwesome, Entypo } from '@expo/vector-icons'

function Button({iconName, onPress}) {
    return (
        <TouchableOpacity onPressOut={onPress}>
            <FontAwesome name={iconName} size={80} color='white' />            
        </TouchableOpacity>
    );
}

Button.proptypes = {
    iconName: PropTypes.string.isRequired,
    onPress: PropTypes.string.isRequired
}

export default Button;